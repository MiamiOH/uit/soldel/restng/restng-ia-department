<?php
/*
-----------------------------------------------------------
FILE NAME: getDepartmentTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION:

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit

TABLE USAGE:

Web Service Usage:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

05/31/2016              millse
Description:            Initial Draft
			 
-----------------------------------------------------------
*/

namespace MiamiOH\RestngIaDepartment\Tests\Unit;

class GetDepartmentTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $api, $request, $dbh, $department;


    // set up method automatically called by PHPUnit before every test method:

    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->department = new \MiamiOH\RestngIaDepartment\Services\Standardized();
        $this->department->setDatabase($db);
        $this->department->setLogger();
        $this->department->setDatasource($ds);
        $this->department->setRequest($this->request);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
	 *   Tests Case in which no option is not specified.
	 */
    public function testNoFilter()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoOptions')));
        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllDepartments')));

        $resp = $this->department->getDepartments();
        $this->assertEquals($this->mockExpectedAllDepartmentsResult(), $resp->getPayload());
    }

    public function mockNoOptions()
    {
        return array();
    }

    public function mockNoResourceParams()
    {
        return array();
    }

    public function mockQueryAllDepartments()
    {
        return array(
            array(
                'standardized_department_cd' => 'PSY',
                'standardized_department_nm' => 'Psychology',
                'standardized_division_cd' => 'CAS',
                'standardized_division_nm' => 'College of Arts & Science'
            ),
            array(
                'standardized_department_cd' => 'POL',
                'standardized_department_nm' => 'Political Science',
                'standardized_division_cd' => 'CAS',
                'standardized_division_nm' => 'College of Arts & Science'
            ),
            array(
                'standardized_department_cd' => 'ECE',
                'standardized_department_nm' => 'Electrical & Computer Engineering',
                'standardized_division_cd' => 'CEC',
                'standardized_division_nm' => 'College of Engineering & Computing'
            )
        );
    }

    public function mockExpectedAllDepartmentsResult()
    {
        return array(
            array(
                'departmentCode' => 'PSY',
                'departmentName' => 'Psychology',
                'divisionCode' => 'CAS',
                'divisionName' => 'College of Arts & Science'
            ),
            array(
                'departmentCode' => 'POL',
                'departmentName' => 'Political Science',
                'divisionCode' => 'CAS',
                'divisionName' => 'College of Arts & Science'
            ),
            array(
                'departmentCode' => 'ECE',
                'departmentName' => 'Electrical & Computer Engineering',
                'divisionCode' => 'CEC',
                'divisionName' => 'College of Engineering & Computing'
            )
        );
    }

    /*
     * Test case in which a valid departmentCode filter is used.
     */
    public function testDepartmentCodeFilter()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockDepartmentCodeOption')));
        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryDepartmentsWithDepartmentCode')));

        $resp = $this->department->getDepartments();
        $this->assertEquals($this->mockExpectedDepartmentsWithDepartmentCodeResult(), $resp->getPayload());
    }

    public function mockDepartmentCodeOption()
    {
        return array(
            'departmentCode' => 'ECE'
        );
    }

    public function mockQueryDepartmentsWithDepartmentCode()
    {
        return array(
            array(
                'standardized_department_cd' => 'ECE',
                'standardized_department_nm' => 'Electrical & Computer Engineering',
                'standardized_division_cd' => 'CEC',
                'standardized_division_nm' => 'College of Engineering & Computing'
            )
        );
    }

    public function mockExpectedDepartmentsWithDepartmentCodeResult()
    {
        return array(
            array(
                'departmentCode' => 'ECE',
                'departmentName' => 'Electrical & Computer Engineering',
                'divisionCode' => 'CEC',
                'divisionName' => 'College of Engineering & Computing'
            )
        );
    }

    /*
     * Test case in which an empty departmentCode filter is used.
     */
    public function testDepartmentCodeFilterEmpty()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockDepartmentCodeOptionEmpty')));
        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));
        try {
            $resp = $this->department->getDepartments();
        } catch (\Exception $e) {
            $this->assertEquals('Error: departmentCode is empty', $e->getMessage());
        }
    }

    public function mockDepartmentCodeOptionEmpty()
    {
        return array(
            'departmentCode' => ''
        );
    }

    /*
     * Test case in which an invalid/sql injection departmentCode filter is used.
     */
    public function testDepartmentCodeFilterInvalid()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockDepartmentCodeOptionInvalid')));
        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));
        try {
            $resp = $this->department->getDepartments();
        } catch (\Exception $e) {
            $this->assertEquals('Error: departmentCode is invalid', $e->getMessage());
        }
    }

    public function mockDepartmentCodeOptionInvalid()
    {
        return array(
            'departmentCode' => 'delete * from users;'
        );
    }

    /*
     * Test case in which a valid divisionCode filter is used.
     */
    public function testDivisionCodeFilter()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockDivisionCodeOption')));
        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryDepartmentsWithDivisionCode')));

        $resp = $this->department->getDepartments();
        $this->assertEquals($this->mockExpectedDepartmentsWithDivisionCodeResult(), $resp->getPayload());
    }

    public function mockDivisionCodeOption()
    {
        return array(
            'divisionCode' => 'CAS'
        );
    }

    public function mockQueryDepartmentsWithDivisionCode()
    {
        return array(
            array(
                'standardized_department_cd' => 'PSY',
                'standardized_department_nm' => 'Psychology',
                'standardized_division_cd' => 'CAS',
                'standardized_division_nm' => 'College of Arts & Science'
            ),
            array(
                'standardized_department_cd' => 'POL',
                'standardized_department_nm' => 'Political Science',
                'standardized_division_cd' => 'CAS',
                'standardized_division_nm' => 'College of Arts & Science'
            )
        );
    }

    public function mockExpectedDepartmentsWithDivisionCodeResult()
    {
        return array(
            array(
                'departmentCode' => 'PSY',
                'departmentName' => 'Psychology',
                'divisionCode' => 'CAS',
                'divisionName' => 'College of Arts & Science'
            ),
            array(
                'departmentCode' => 'POL',
                'departmentName' => 'Political Science',
                'divisionCode' => 'CAS',
                'divisionName' => 'College of Arts & Science'
            )
        );
    }

    /*
     * Test case in which an empty divisionCode filter is used.
     */
    public function testDivisionCodeFilterEmpty()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockDivisionCodeOptionEmpty')));
        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));
        try {
            $resp = $this->department->getDepartments();
        } catch (\Exception $e) {
            $this->assertEquals('Error: divisionCode is empty', $e->getMessage());
        }
    }

    public function mockDivisionCodeOptionEmpty()
    {
        return array(
            'divisionCode' => ''
        );
    }

    /*
     * Test case in which an invalid/sql injection divisionCode filter is used.
     */
    public function testDivisionCodeFilterInvalid()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockDivisionCodeOptionInvalid')));
        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockResourceParams')));
        try {
            $resp = $this->department->getDepartments();
        } catch (\Exception $e) {
            $this->assertEquals('Error: divisionCode is invalid', $e->getMessage());
        }
    }

    public function mockDivisionCodeOptionInvalid()
    {
        return array(
            'divisionCode' => 'delete * from users;'
        );
    }
}