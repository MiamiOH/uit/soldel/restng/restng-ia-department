# Test access to the Two Factor REST API
Feature: Get user TFA status
  As a consumer of the TFA status service
  I want to get information about the status of a user
  In order to know their enrollment and opt-in status

  Scenario: Get data about a user
    Given a REST client
    When I make a GET request for /basic/something/1
    Then the HTTP status code is 200
    And the HTTP Content-Type header is "application/json"
    And the response data element contains a id key matching "1"

