<?php

namespace MiamiOH\RestngIaDepartment\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class StandardizedResourceProvider extends ResourceProvider
{

    private $tag = "IA";
    private $dot_path = "IA.Department.Standardized";
    private $s_path = "/ia/department/standardized/v1";
    private $bs_path = '\Standardized';

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'IA',
            'description' => 'Resources from IA (GET only)'
        ));


        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Department',
            'type' => 'object',
            'properties' => array(
                'departmentCode' => array('type' => 'string', 'description' => 'Code of the department'),
                'departmentDescription' => array('type' => 'string', 'description' => 'Description/name of the department'),
                //'departmentDivisionCode' => array('type' => 'string', 'description' => 'Code of the division the department belongs to'),
                //'departmentDivisionDescription' => array('type' => 'string', 'description' => 'Description of the division the department belongs to'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Department.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Department',
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Standardized',
            'class' => 'MiamiOH\RestngIaDepartment\Services' . $this->bs_path,
            'description' => 'This service provides resources about Departments from the IA data source.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->dot_path . '.get',
                'description' => 'Return all Standardized Departments.',
                'pattern' => $this->s_path,
                'service' => 'Standardized',
                'method' => 'getDepartments',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'options' => array(
                    'departmentCode' => array('type' => 'single', 'description' => 'Department code by which to filter'),
                    //'divisionCode' => array('type' => 'single', 'description' => 'Division code by which to filter the departments'),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Departments, their codes and names as well as the division code and name the department belongs to',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Department.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}