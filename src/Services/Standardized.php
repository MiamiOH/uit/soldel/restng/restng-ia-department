<?php

/*
	-----------------------------------------------------------
	FILE NAME: Department.class.php

	Copyright (c) 2016 Miami University, All Rights Reserved.

	Miami University grants you ("Licensee") a non-exclusive, royalty free,
	license to use, modify and redistribute this software in source and
	binary code form, provided that i) this copyright notice and license
	appear on all copies of the software; and ii) Licensee does not utilize
	the software in a manner which is disparaging to Miami University.

	This software is provided "AS IS" and any express or implied warranties,
	including, but not limited to, the implied warranties of merchantability
	and fitness for a particular purpose are disclaimed. It has been tested
	and is believed to work as intended within Miami University's
	environment. Miami University does not warrant this software to work as
	designed in any other environment.

	AUTHOR: Erin Mills

	DESCRIPTION:  The Department service is currently designed for GET only

	INPUT:
	PARAMETERS:

	ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

	TABLE USAGE:

	AUDIT TRAIL:

	DATE    PRJ-TSK          UniqueID
	Description:

	05/27/2016		        millse
	Description:	        Initial Program
	
	09/27/2016		millse
	Description:		Remove division info because it is causing multiple departments to be returned.
	
 */

namespace MiamiOH\RestngIaDepartment\Services;

class Standardized extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $database = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_GEN_IAPROD'; // secure datasource


    /************************************************/
    /**********Setter Dependency Injection***********/
    /***********************************************/

    // Inject the datasource object provided by the framework
    public function setDataSource($datasource)
    {
        $this->dataSource = $datasource;
    }

    // Inject the database object provided by the framework
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    // Inject the configuration object provided by the framework
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    // GET(read/view) the Department Information
    public function getDepartments()
    {

        //log
        $this->log->debug('Department Standardized service was called.');

        //set up some variables for use later
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $departmentCode = null;
        $divisionCode = null;

        //check the departmentCode option if one was set
        if (isset($options['departmentCode'])) {
            if ($options['departmentCode'] == '') {
                throw new \Exception('Error: departmentCode is empty');
            }
            if (preg_match('/[^A-Za-z0-9() :\-_"\']/', $options['departmentCode'])) {
                throw new \Exception('Error: departmentCode is invalid');
            }
            $departmentCode = $options['departmentCode'];
        }

        //the query we'll be running
//         $queryString = "
//             select
//               distinct
//               standardized_department_cd,
//               standardized_department_nm
//             from Mudwmgr.Dim_Std_Division_Department
//             where standardized_department_cd != 'NA'
//             and effective_from_dt < sysdate
//             and effective_to_dt > sysdate
//         ";

        $queryString = "
            select distinct
            	standardized_department_cd,
            	standardized_department_nm
            from Mudwmgr.Ref_standardize_department
            where standardized_department_cd != 'NA'
            and effective_from_dt < sysdate
            and effective_to_dt > sysdate
        ";

        //if a valid departmentCode option was set, add a bit to the query to filter by departmentCode
        if ($departmentCode) {
            $queryString .= ' and standardized_department_cd = ?';
        }

        $dbh = $this->database->getHandle($this->datasource_name);

        //if we have only a departmentCode to filter by
        if ($departmentCode) { //if we have a department code to filter by
            $results = $dbh->queryall_array($queryString, $departmentCode);
        } else { //if we aren't doing any filtering
            $results = $dbh->queryall_array($queryString);
        }

        //send the db results to the makeover method and stuff them into the $payload
        $payload = $this->departmentMakeover($results);
        //$payload = $results;

        // Response was successful, Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;
    }

    //helper method to change the db column names into pretty names for the web service response
    private function departmentMakeover($dbResults)
    {
        $prettyDepartments = array(); //the array of data to return that has been pretty-fied
        foreach ($dbResults as $record) {
            $tmp = array();//temporary array to hold the pretty values from each db record returned
            $tmp['departmentCode'] = $record['standardized_department_cd'];
            $tmp['departmentName'] = $record['standardized_department_nm'];
            $prettyDepartments[] = $tmp;//add each db record that has been pretty-fied into the return array
        }
        return $prettyDepartments;//return!
    }
}
